# Batch checker for BI-PA* homework

This humble, yet powerful, script will hopefully help you to check all your code against given examples.

As first extension, this script will also check time and memory requirements on your machine (which can wildly differ from Progtest!)

Anyone is welcomed to use this script and extend it to any amount. Yet if you find your extension helpful, please make a merge request with your feature!