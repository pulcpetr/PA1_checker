#!/bin/bash

if [[ $# < 1 ]]
then
  echo "Usage: $0 <executable> [tests_folder]";
  echo "Example: $0 hw/a.out hw/tests"
  echo "Example: $0 ./a.out"
  exit;
fi

if [[ $# < 2 ]]
then
  path="."
else
  path=$2
fi

for f in $path/*_in.txt
do
  number=`basename $f _in.txt`;
  echo "Test number $number:";
 
  echo "=> Output test:";
  $1 < $f | diff -s - $path/$number"_out.txt";

  echo ""
  echo "=> Time and memory test:";
  /usr/bin/time -f "time: %E, memory: %KkB" $1 <$f >/dev/null

  echo ""
done